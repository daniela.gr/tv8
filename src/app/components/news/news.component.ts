import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  articles = [];
  perPage = 15;
  nrOfPage = 1;

  constructor(private apiService: ApiService, private spinner: NgxSpinnerService, private globalService: GlobalService) {
  }

  ngOnInit(): void {
    this.getNews();
  }

  getNews(): void {
    this.apiService.getInfo(this.perPage, this.nrOfPage).subscribe(data => {
      data.map(el => {
        if (el.video_embed) {
          el.video_embed = 'https://www.youtube.com/embed/' + this.getId(el.video_embed);
          console.log('el.video_embed', el.video_embed);
        }
      });
      this.articles.push(...data);
      console.log(this.articles);
    });
  }

  getId(url): string {
    return this.globalService.getId(url);
  }

  onScroll(): void {
    this.spinner.show();
    this.nrOfPage++;
    this.getNews();
    console.log('scrolled!!');
  }

  getDate(date): string {
    return this.globalService.getDate(date);
  }
}

