import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {GlobalService} from "../../services/global.service";
import {faArrowAltCircleLeft} from '@fortawesome/free-solid-svg-icons';
import {Location} from '@angular/common';

@Component({
  selector: 'app-single-page',
  templateUrl: './single-page.component.html',
  styleUrls: ['./single-page.component.css']
})
export class SinglePageComponent implements OnInit {
  newsId;
  article;
  media;
  faArrow = faArrowAltCircleLeft;

  constructor(private route: ActivatedRoute,
              private apiService: ApiService,
              private globalService: GlobalService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.newsId = this.route.snapshot.params.id;
    this.apiService.getNewsById(this.newsId).subscribe(data => {
      this.article = data;
      if (data.video_embed) {
        data.video_embed = 'https://www.youtube.com/embed/' + this.getId(data.video_embed);
      }
      this.apiService.getMedia(data._links['wp:featuredmedia'][0].href).subscribe(mediaData => {
        this.media = mediaData;
      });

      console.log(this.article);
    });
  }

  getDate(date): string {
    return this.globalService.getDate(date);
  }

  getId(url): string {
    return this.globalService.getId(url);
  }

  goBack(): void {
    this.location.back();
  }
}
