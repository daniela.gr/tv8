import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) {
  }

  public getInfo(perPage, nrOfPage): any {
    return this.httpClient.get(`https://tv8.md/wp-json/wp/v2/posts?_embed&per_page=${perPage}&page=${nrOfPage}`);
  }

  public getNewsById(id): any {
    return this.httpClient.get(`https://tv8.md/wp-json/wp/v2/posts/${id}`);
  }

  public getMedia(url): any {
    return this.httpClient.get(url);
  }
}
