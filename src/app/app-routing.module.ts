import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NewsComponent} from './components/news/news.component';
import {SinglePageComponent} from './components/single-page/single-page.component';


const routes: Routes = [
  {
    path: '', redirectTo: 'news', pathMatch: 'full'
  },
  {
    path: 'news', component: NewsComponent
  },
  {
    path: 'news/:id', component: SinglePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

